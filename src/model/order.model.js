/*
* 订单模块
* */
const { DataTypes } = require('sequelize');
const seq = require('../db/seq');

const Order = seq.define('node_order',{
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        comment: '用户id'
    },
    address_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        comment: "用户地址id"
    },
    goods_info: {
        type: DataTypes.TEXT,
        allowNull: false,
        comment: "订单商品信息"
    },
    total: {
        type: DataTypes.DECIMAL(10,2),
        allowNull: false,
        comment: "订单总金额"
    },
    order_num: {
        type: DataTypes.CHAR(17),
        allowNull: false,
        comment: "订单号",
        unique: true           //唯一
    },
    status: {
        type: DataTypes.TINYINT,
        allowNull: false,
        comment: "订单状态",               //0 未付款 1 已付款 2 待发货 3 已发货  4 已签收
        defaultValue: '0'
    }
},{paranoid: true}
);

// Order.sync({force: true});

module.exports = Order;
