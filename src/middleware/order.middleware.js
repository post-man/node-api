const validatorParamsOrQuery = (rules) => {
    return async (ctx,next) => {
        try{
            let params;
            if(rules == 'create'){
                params = {
                    address_id: {type: 'string', required: true},
                    goods_info: {type: 'string', required: true},
                    total: {type: 'string', required: true},
                }
            } else if(rules == 'delete'){
                params = {
                    orderId: {type: 'string', required: true}
                }
            } else if(rules == 'update'){
                params = {
                    address_id: {type: 'string', required: true},
                    goods_info: {type: 'string', required: true},
                    total: {type: 'string', required: true},
                }
            }
            ctx.verifyParams(params);
        }catch (e) {
            console.log("订单模块参数出现错误",ctx.request.body);
            return  ctx.body = {
                code: '0001',
                message: '订单参数错误',
                result: e
            }
        }

        await next();
    }
}

module.exports = {
    validatorParamsOrQuery
}