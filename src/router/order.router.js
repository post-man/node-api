/*
* 订单模块
* */
const Router = require('koa-router');

const {auth} = require('../middleware/auth.middleware');
const {validatorParamsOrQuery} = require('../middleware/order.middleware');
const {createOrderController, deleteOrderController, getOrdersController, updateOrderController} = require('../controller/order.controller');

const orderRouter = new Router({prefix: '/order'});

orderRouter.post('/createOrder',auth, validatorParamsOrQuery('create'), createOrderController);

orderRouter.post('/updateOrder/:id',auth, validatorParamsOrQuery('update'), updateOrderController);

orderRouter.post('/deleteOrder',auth, validatorParamsOrQuery('delete'), deleteOrderController);

orderRouter.get('/',auth, getOrdersController);

module.exports = orderRouter