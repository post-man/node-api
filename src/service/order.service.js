const Order = require('../model/order.model');

class OrderService {
    async createOrderService(obj){
        let res = await Order.create(obj);
        return res;
    }

    async deleteOrderService(id){
        let res = await Order.destroy({
            where: {id},
            force: true
        });
        return res>0 ? true : false;
    }

    async getOrderService(user_id){
        let res = await Order.findAll({
            where: {user_id}
        });
        let orders = res.map(item => {
            let {dataValues} = item;
            return dataValues
        });
        return {
            list: orders
        }
    }

    async updateOrderService(id,obj){
        let res = await Order.update(obj,{
            where: {id}
        });
        if(res[0] > 0){
            let newObj = await Order.findOne({
                attributes: ['id'],
                where: {id}
            });
            return newObj;
        } else {
            return false;
        }
    }
}

module.exports = new OrderService();