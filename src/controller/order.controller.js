/*
* 订单模块
* */
const { body } = require('koa/lib/response');
const {createOrderService, deleteOrderService, getOrderService, updateOrderService} = require('../service/order.service');
class OrderController {
    async createOrderController(ctx){
        let user_id = ctx.state.user.id;
        let {address_id,goods_info,total} = ctx.request.body;
        let order_num = "No"+Date.now()+"De";
        try{
            let res = await createOrderService({user_id,address_id,goods_info,total,order_num});
            ctx.body = {
                code: '0000',
                message: '订单生成成功',
                result: res
            }
        } catch (e) {
            console.error(e,"生成订单报错");
            ctx.app.emit('error',{
                code: "1001",
                message: "服务器出错，生成订单失败"
            },ctx);
        }
    }

    async deleteOrderController(ctx){
        let id = ctx.request.body.orderId;
        try {
            let res = await deleteOrderService(id);
            if(res){
                ctx.body = {
                    code: '0000',
                    message: "订单删除成功"
                }
            } else {
                ctx.body = {
                    code: '0001',
                    message: "删除失败，无效id"
                }
            }
        } catch (error) {
            console.error(error,"删除订单报错");
            ctx.app.emit('error',{
                code: "1001",
                message: "服务器出错，删除订单失败"
            },ctx);
        }
    }

    async getOrdersController(ctx){
        let user_id = ctx.state.user.id;
        try {
            let res = await getOrderService(user_id);
            ctx.body = {
                code: '0000',
                message: "获取订单列表成功",
                result: res
            }
        } catch (error) {
            console.error(error,"获取订单报错");
            ctx.app.emit('error',{
                code: "1001",
                message: "服务器出错，获取订单失败"
            },ctx);
        }
    }

    async updateOrderController(ctx){
        let user_id = ctx.state.user.id;
        let id = ctx.params.id;
        let {address_id,goods_info,total} = ctx.request.body;
        let order_num = "No"+Date.now()+"De";
        try{
            let res = await updateOrderService(id,{user_id,address_id,goods_info,total,order_num});
            if(res){
                ctx.body = {
                    code: '0000',
                    message: "订单更新成功",
                    result: res
                };
            } else {
                ctx.body = {
                    code: '0001',
                    message: "订单更新失败",
                    result: {}
                };
            };
        }catch (error) {
            console.error(error,"更新订单报错");
            ctx.app.emit('error',{
                code: "1001",
                message: "服务器出错，更新订单失败"
            },ctx);
        }
    }
}

module.exports = new OrderController();